<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEtablissementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('etablissements', function(Blueprint $table)
		{
			$table->foreign('sirene', 'etablissements_ibfk_1')->references('sirene')->on('societes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('apet700', 'etablissements_ibfk_2')->references('ape')->on('ape')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tefet', 'etablissements_ibfk_3')->references('efet')->on('effectifs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('etablissements', function(Blueprint $table)
		{
			$table->dropForeign('etablissements_ibfk_1');
			$table->dropForeign('etablissements_ibfk_2');
			$table->dropForeign('etablissements_ibfk_3');
		});
	}

}

@extends('layouts.app')

@section('content')

<div class="container">
	<h1>Liste des Entreprises</h1>
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th scope="col">Sirène</th>
	      <th scope="col">Nom socièté</th>
	      <th scope="col">Adresse</th>
	      <th scope="col">NAF</th>
	    </tr>
	  </thead>
	  <tbody>

@foreach($reg as $objet)
		<tr>
	      <th scope="row">{{ $objet->sirene}}</th>
	      <td>{{ $objet->nomen_long }}</td>
	      <td>{{ $objet->nicsiege }}</td>
	      <td>{{ $objet->rpen}}</td>
	    </tr>
@endforeach
	  </tbody>
	</table>
</div>
@endsection
package com.ppe.siren;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe Qui Recupere des donn�es de la Base Siren et les r�injectes dans une nouvelle base.
 * 
 * @author Brandon
 * @version 1.0
 */

public class ConnexionDB {
	
	private Connection connexionBaseProf = null;
	private Connection connexionBaseEleve = null;
	
	private String requeteEntreprise = "insert into societes (sirene,nomen_long,sigle,nom,prenom,civilite,rna,nicsiege,rpen,depcomen,adr_mail,nj,apen700,dapen,aprm,essen,dateess,tefen,efencent,defen,categorie,dcren,amintren,monoact,moden,proden,esaann,tca,esaapen,esasec1n,esasec2n,esasec3n,esasec4n,datemaj) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sirene=?";
	private String requeteTable2="insert into etablissements (sirene,nic,l1_normalisee,l2_normalisee,l3_normalisee,l4_normalisee,l5_normalisee,l6_normalisee,l7_normalisee,l1_declaree,l2_declaree,l3_declaree,l4_declaree,l5_declaree,l6_declaree,l7_declaree,numvoie,indrep,typvoie,libvoie,codpos,cedex,rpet,libreg,depet,arronet,ctonet,comet,libcom,du,tu,uu,epci,tcd,zemet,siege,enseigne,ind_publipo,diffcom,amintret,natetab,apet700,dapet,tefet,efetcent,defet,origine,dcret,date_deb_etat_adm_et,activnat,lieuact,actisurf,saisonat,modet,prodet,prodpart,auxilt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sirene=? , nic =?";
	private String requeteTable3="insert into nature (nj,libnj) values (?,?) ON DUPLICATE KEY UPDATE nj = ?";
	private String requeteTable4="insert into effectifs (efet,libefet) values (?,?) ON DUPLICATE KEY UPDATE efet = ?";
	private String requeteTable5="insert into ape (ape,libape) values(?,?) ON DUPLICATE KEY UPDATE ape = ?";
	private String requeteTable6="insert into natetab (natetab,libnatetab) values (?,?) ON DUPLICATE KEY UPDATE natetab = ?";
	
	private PreparedStatement prep1;
	private PreparedStatement prep2;
	private PreparedStatement prep3;
	private PreparedStatement prep4;
	private PreparedStatement prep5;
	private PreparedStatement prep6;
	private PreparedStatement prepSelection;
	private String requeteSelection = "select * from entreprises LIMIT 100  OFFSET ? ";
	
	public ConnexionDB() throws SQLException {
		
		connectToFirstBaseProf();
		connectToBaseEleve();
		initStatement();
		

	}
	
	public void initStatement() { 
		 try {
			prep1 = getConnexionBaseEleve().prepareStatement(requeteEntreprise);
			prep2 = getConnexionBaseEleve().prepareStatement(requeteTable2);
			prep3 = getConnexionBaseEleve().prepareStatement(requeteTable3);
			prep4 = getConnexionBaseEleve().prepareStatement(requeteTable4);
			prep5 = getConnexionBaseEleve().prepareStatement(requeteTable5);
			prep6 = getConnexionBaseEleve().prepareStatement(requeteTable6);
			prepSelection = getConnexionBaseProf().prepareStatement(requeteSelection);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	/* Recupere les Donn�es de la Base Professeur et les envoies a la methode d'injection
	 * 
	 * 
	 */
	@SuppressWarnings("resource")
	public void getData()  {
		ResultSet resulset;
		int sizeQuery = 13500;
		System.out.println("Hello");
		try {
			
			File file = new File("Compteur.txt");
			BufferedReader fichier = new BufferedReader(new FileReader(file));
			BufferedWriter write;
			String test = fichier.readLine();
			
			// Recupere la progression dans le fichier 
			
			if(file.exists() && test != null) {
				sizeQuery = Integer.parseInt(test);
				fichier.close();
				
			}else {
				fichier.close();
			
				sizeQuery = 0;
			}
			
			// permet de savoir quel block on a parcouru
			int compteur = sizeQuery / 100;
			
			while(sizeQuery <= 11000000) {
				
				prepSelection.setInt(1, sizeQuery);
				resulset = prepSelection.executeQuery();
	
				injectionToDbEleve(resulset);
				
				sizeQuery += 100;
				compteur++;
				
				write = new BufferedWriter(new PrintWriter(file));
				write.write(""+ sizeQuery);
				write.close();
				
				System.out.println("Bloc " + compteur + " Effectué !");
				System.out.println("Il reste " + (12000000 - sizeQuery) / 100 + " Blocks a effectuer ! \n");
				
			}
		
		}catch (SQLException | IOException e) {
			
			System.err.println(e.getMessage());
			// On change de block pour traiter les erreurs
			sizeQuery += 100;
		}
	}
	
	/* Met a jour les données dans la base Eleve
	 * 
	 * @param result 
	 * 				Objet ResulSet Contenant les entreprises a injecter
	 * 
	 */
	public void injectionToDbEleve(ResultSet result) throws SQLException {
		
		//PreparedStatement prep7 = getConnexionBaseEleve().prepareStatement(requeteTable6);
	
		while(result.next()) {
			try {
				//nature
				prep3.setString(1, result.getString("nj"));
				prep3.setString(2, result.getString("libnj"));
				prep3.setString(3, result.getString("nj"));
				prep3.executeUpdate();
				
				//effectifs
				prep4.setString(1, result.getString("tefet"));
				prep4.setString(2, result.getString("libtefet"));
				prep4.setString(3, result.getString("tefet"));
				prep4.executeUpdate();
				//effectifs
				prep4.setString(1, result.getString("tefen"));
				prep4.setString(2, result.getString("libtefen"));
				prep4.setString(3, result.getString("tefen"));
				prep4.executeUpdate();
				
			
				
				// ape
				prep5.setString(1, result.getString("apen700"));
				prep5.setString(2, result.getString("libapen"));
				prep5.setString(3, result.getString("apen700"));
				prep5.executeUpdate();
				
				// ape
				prep5.setString(1, result.getString("apet700"));
				prep5.setString(2, result.getString("libapet"));
				prep5.setString(3, result.getString("apet700"));
				prep5.executeUpdate();
				
				//natetab
				prep6.setString(1, result.getString("natetab"));
				prep6.setString(2, result.getString("libnatetab"));
				prep6.setString(3, result.getString("natetab"));
				
				prep6.executeUpdate();
				
				
				//table societes
				prep1.setString(1, result.getString("sirene"));
				prep1.setString(2, result.getString("nomen_long"));
				prep1.setString(3, result.getString("sigle"));
				prep1.setString(4, result.getString("nom"));
				prep1.setString(5, result.getString("prenom"));
				prep1.setString(6, result.getString("civilite"));
				prep1.setString(7, result.getString("rna"));
				prep1.setString(8, result.getString("nicsiege"));
				prep1.setString(9, result.getString("rpen"));
				prep1.setString(10, result.getString("depcomen"));
				prep1.setString(11, result.getString("adr_mail"));
				prep1.setString(12, result.getString("nj"));
				prep1.setString(13, result.getString("apen700"));
				prep1.setString(14, result.getString("dapen"));
				prep1.setString(15, result.getString("aprm"));
				prep1.setString(16, result.getString("essen"));
				prep1.setString(17, result.getString("dateess")); // ICI
				prep1.setString(18, result.getString("tefen"));
				prep1.setString(19, result.getString("efetcent"));
				prep1.setString(20, result.getString("defen"));
				prep1.setString(21, result.getString("categorie"));
				prep1.setString(22, result.getString("dcren"));
				prep1.setString(23, result.getString("amintren"));
				prep1.setString(24, result.getString("monoact"));
				prep1.setString(25, result.getString("moden"));
				prep1.setString(26, result.getString("proden"));
				prep1.setString(27, result.getString("esaann"));
				prep1.setString(28, result.getString("tca"));
				prep1.setString(29, result.getString("esaapen"));
				prep1.setString(30, result.getString("esasec1n"));
				prep1.setString(31, result.getString("esasec2n"));
				prep1.setString(32, result.getString("esasec3n"));
				prep1.setString(33, result.getString("esasec4n"));
				prep1.setString(34, result.getString("datemaj"));
				prep1.setString(35, result.getString("sirene"));
				
				prep1.executeUpdate();
				// table etablissements
				prep2.setString(1, result.getString("sirene"));
				prep2.setString(2, result.getString("nic"));
				prep2.setString(3, result.getString("l1_normalisee"));
				prep2.setString(4, result.getString("l2_normalisee"));
				prep2.setString(5, result.getString("l3_normalisee"));
				prep2.setString(6, result.getString("l4_normalisee"));
				prep2.setString(7, result.getString("l5_normalisee"));
				prep2.setString(8, result.getString("l6_normalisee"));
				prep2.setString(9, result.getString("l7_normalisee"));
				prep2.setString(10, result.getString("l1_declaree"));
				prep2.setString(11, result.getString("l2_declaree"));
				prep2.setString(12, result.getString("l3_declaree"));
				prep2.setString(13, result.getString("l4_declaree"));
				prep2.setString(14, result.getString("l5_declaree"));
				prep2.setString(15, result.getString("l6_declaree"));
				prep2.setString(16, result.getString("l7_declaree"));
				prep2.setString(17, result.getString("numvoie"));
				prep2.setString(18, result.getString("indrep"));
				prep2.setString(19, result.getString("typvoie"));
				prep2.setString(20, result.getString("libvoie"));
				prep2.setString(21, result.getString("codpos"));
				prep2.setString(22, result.getString("cedex"));
				prep2.setString(23, result.getString("rpet"));
				prep2.setString(24, result.getString("libreg"));
				prep2.setString(25, result.getString("depet"));
				prep2.setString(26, result.getString("arronet"));
				prep2.setString(27, result.getString("ctonet"));
				prep2.setString(28, result.getString("comet"));
				prep2.setString(29, result.getString("libcom"));
				prep2.setString(30, result.getString("du"));
				prep2.setString(31, result.getString("tu"));
				prep2.setString(32, result.getString("uu"));
				prep2.setString(33, result.getString("epci"));
				prep2.setString(34, result.getString("tcd"));
				prep2.setString(35, result.getString("zemet"));
				prep2.setString(36, result.getString("siege"));
				prep2.setString(37, result.getString("enseigne"));
				prep2.setString(38, result.getString("ind_publipo"));
				prep2.setString(39, result.getString("diffcom"));
				prep2.setString(40, result.getString("amintret"));
				prep2.setString(41, result.getString("natetab"));
				prep2.setString(42, result.getString("apet700"));
				prep2.setString(43, result.getString("dapet"));
				prep2.setString(44, result.getString("tefet"));
				prep2.setString(45, result.getString("efetcent"));
				prep2.setString(46, result.getString("defet"));
				prep2.setString(47, result.getString("origine"));
				prep2.setString(48, result.getString("dcret"));
				prep2.setString(49, result.getString("date_deb_etat_adm_et"));
				prep2.setString(50, result.getString("activnat"));
				prep2.setString(51, result.getString("lieuact"));
				prep2.setString(52, result.getString("actisurf"));
				prep2.setString(53, result.getString("saisonat"));
				prep2.setString(54, result.getString("modet"));
				prep2.setString(55, result.getString("prodet"));
				prep2.setString(56, result.getString("prodpart"));
				prep2.setString(57, result.getString("auxilt"));
				prep2.setString(58, result.getString("sirene"));
				prep2.setString(59, result.getString("nic"));
				
				prep2.executeUpdate();
				
			}catch (SQLException e) {
				System.out.println(e.getMessage());
			}
	
		}
			
	}

	/*Methode Permettant d'initialiser la connexion Vers la Base Professeur
	 * 
	 */
	public Connection connectToFirstBaseProf() {
		// CONNEXION BASE PROFESSEUR
		String url = "jdbc:postgresql://172.22.212.73:5432/siren";
		String password = "siren";
		String user = "siren";
		
		try {
			connexionBaseProf = DriverManager.getConnection(url,user,password);
			
			System.out.println("Connexion Prof réussi !");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connexionBaseProf;
	}
	
	
	/*Methode Permettant d'initialiser la connexion Vers la Base Eleve
	 * 
	 */
	public Connection connectToBaseEleve() {
		//ConnexionBase GROUPE PPE
		String url1 = "jdbc:mysql://172.22.212.30:3306/SIRENE?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String user1 = "userext";
		String password1 = "userextmdp";
		
		
		try {
			connexionBaseEleve = DriverManager.getConnection(url1,user1,password1);
			System.err.println("Connexion Base Localhost Reussi !");
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connexionBaseEleve;
	}
	
	/* Recupere l'objet Connexion pour la Base du Professeur
	 * 
	 */
	public Connection getConnexionBaseProf() {
		return connexionBaseProf;
	}

	
	/* Recupere l'objet Connexion de la Base Eleve
	 * 
	 * @return l'objet Connexion
	 */
	public Connection getConnexionBaseEleve() {
		return connexionBaseEleve;
	}

	
	/*
	 * Ferme la Connexion vers la Base de l'eleve
	 * 
	 * @return l'objet Connexion Null
	 * 
	 */
	public Connection closeConnexionEleve() throws SQLException { 
		if(getConnexionBaseProf()!= null) {
			connexionBaseProf.close();	
		}
		return connexionBaseProf;
	}
	
	/*
	 * Ferme la Connexion vers la Base du Prof
	 * 
	 * @return l'objet Connexion Null
	 */
	public Connection closeConnexionProf() throws SQLException { 
		if(getConnexionBaseEleve()!= null) {
			connexionBaseEleve.close();	
		}
		return connexionBaseEleve;
	}
	
}

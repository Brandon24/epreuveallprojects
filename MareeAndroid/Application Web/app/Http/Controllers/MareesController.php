<?php

namespace App\Http\Controllers;
//namespace App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use App\Ports;
use App\Horaires;

class MareesController extends Controller
{

  public function index(){
          return view('indexMaree');
}

public function visualisation(){
  $posts = Ports::all();
  return view('visualisation',compact('posts'));
}

public function supprimer($id){
  Marees::where('id',$id)->delete();
  return redirect()->route('visualisation');

}
  // Route::get('/visualisation', 'MareesController@visualisation')->name('visualisation')->middleware('auth');
public function ajouter(Request $request){
  //  $request→validate([ 'nomPort' => 'required' ])
  // $request->validate([ 'nomPort' => 'required|string' ,
  //         'date' => 'required|date',
  //       //   'HPM' => 'required|date',
  //         // 'HBM' => 'required|date',
  //       //  date_format:"H:i"|required
  //         'EPM' => 'required|integer','EDM' => 'required|integer'
  //         ]);
  // $validator = Validator::make($request->all(), [
  //          'port' => 'required',
  //       //   'Date_Maree' => 'bail|required|date',
  //          //'HPM' => 'bail|required|time',
  //       //   'HBM' => 'bail|required|time',
  //   //       'EPM' => 'bail|required|integer',
  //     //     'EBM' => 'bail|required|integer',
  //      ]);
       //
       // if ($validator->fails()) {
       //     return back()->withErrors($validator)->withInput();
       //   }

  $ports = new Ports();

  $ports->nom = $request->nomPort;
  $ports->latitude = $request->latitude;
  $ports->longitude = $request->longitude;
  $ports->telCapitainerie = $request->Telephone;
  $ports->save();

// faire requete
  $horaires = new Horaires();
  $portID = Ports::where('nom',$request->nomPort)->first();

  $horaires->idPort = $portID->id;
  $horaires->heurePM = $request->HPM;
  $horaires->heureBM = $request->HBM;
  $horaires->date = $request->date;

  $horaires->eauBM = $request->EBM;
  $horaires->eauPM = $request->EPM;

  $horaires->save();

   return view('ajouteMaree');
  // return '' .  $request->input('nomPort');
}

}

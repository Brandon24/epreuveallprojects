<?php

use Illuminate\Database\Seeder;

class PortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 20; $i++) {
          DB::table('ports')->insert([
           'nom' => str_random(10),
           'latitude' => rand(5, 15),
           'longitude' => rand(5, 15),
           'telCapitainerie' => str_random(10),
         ]);
      }

    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/indexMaree', 'MareesController@index')->name('indexMaree')->middleware('auth');
Route::get('/ajouteMaree', 'MareesController@ajouter')->name('ajouteMaree')->middleware('auth');

Route::get('/visualisation', 'MareesController@visualisation')->name('visualisation')->middleware('auth');

Route::get('/delete/{id}', 'MareesController@supprimer')->name('supprimer');

Route::get('/listeMarees', function() {

  return App\Marees::all();
//  $phpbr = 'KevinLands';
//  return App\Marees::where('port', 'KevinLands');
  //  return DB::table('marees')->where('port', 'Male');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

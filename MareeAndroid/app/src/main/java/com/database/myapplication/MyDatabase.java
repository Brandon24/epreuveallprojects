package com.database.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabase extends SQLiteOpenHelper {

    private final String TABLE_MAREE = "MAREE";
    private final String KEY_ID= "KEY_ID";
    private final String KEY_HPM= "HPM";
    private final String KEY_HBM= "HBM";
    private final String KEY_BPM= "BPM";
    private final String KEY_BBM= "BBM";
    private final String KEY_HFINAL= "Heure_FINAL";
    private final String KEY_EFINAL= "Eau_FINAL";

    /*
    @param name correspond au nom de la base
     */
    public MyDatabase(Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_MAREE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_HPM + " REAL,"
                + KEY_HBM + " REAL,"
                + KEY_HBM + " REAL,"
                + KEY_HBM + "REAL,"
                + KEY_BBM + "REAL,"
                + KEY_HFINAL + "REAL,"
                + KEY_EFINAL + "REAL)";
         db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

package com.comm.fr;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 */

/**
 * @author bzermelwall
 *
 */
public class NoteFrais {
	private Date date;
	private Commercial commercial;
	private double montant;
	
	/**
	 * @param date
	 * @param commercial
	 */
	public NoteFrais(Date date, Commercial commercial) {
		this.date = date;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("");
		this.commercial = commercial;
		commercial.getNoteFrais().add(this);
	}
	
	
	/**
	 * @return the montant
	 */
	public double calculMontant() {
		return 0;
	}
	/**
	 * @param montant the montant to set
	 */
	public void setMontant() {
		this.montant = calculMontant();
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NoteFrais [date=" + date + ", montant a rembourser =" + montant + "]";
	}


	/**
	 * @return the commercial
	 */
	public Commercial getCommercial() {
		return commercial;
	}
	

}

/**
 * 
 */
package com.comm.fr;

import java.util.Date;
import java.util.List;

/**
 * @author bzermelwall
 *
 */
public class NoteNuite extends NoteFrais {
	private int km;


	public NoteNuite(Date date, Commercial commercial,int km) {
		super(date, commercial);
		this.km = km;
		// TODO Auto-generated constructor stub
	}
	
	public double getMontantRembourser() {
		 double prix = 0;
		
		if (getCommercial().getPuissance_voiture() <10 && getCommercial().getPuissance_voiture() >5) {
			prix  = km * 0.1;
		}else if (getCommercial().getPuissance_voiture() > 10) {
			prix  = km *0.2;
		}
		
		return prix;
	}

}

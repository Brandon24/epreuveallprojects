package com.comm.fr;
import java.util.ArrayList;

/**
 * 
 */

/**
 * @author bzermelwall
 *
 */
public class Commercial {
	private String nom;
	private String prenom;
	private int puissance_voiture;
	private char categorie;
	private ArrayList<NoteFrais> noteFrais;
	/**
	 * @param nom
	 * @param prenom
	 * @param puissance_voiture
	 * @param categorie
	 */
	public Commercial(String nom, String prenom, int puissance_voiture, char categorie) {
		this.nom = nom;
		this.prenom = prenom;
		this.puissance_voiture = puissance_voiture;
		this.categorie = categorie;
		noteFrais = new ArrayList<>();
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Commercial [nom=" + nom + ", prenom=" + prenom + ", puissance_voiture=" + puissance_voiture
				+ ", categorie=" + categorie + "]";
	}



	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the puissance_voiture
	 */
	public int getPuissance_voiture() {
		return puissance_voiture;
	}
	/**
	 * @param puissance_voiture the puissance_voiture to set
	 */
	public void setPuissance_voiture(int puissance_voiture) {
		this.puissance_voiture = puissance_voiture;
	}
	/**
	 * @return the categorie
	 */
	public char getCategorie() {
		return categorie;
	}
	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(char categorie) {
		this.categorie = categorie;
	}
	
	public void setNoteDeFrais(NoteFrais note) {
		noteFrais.add(note);
		
	}

	/**
	 * @return the noteFrais
	 */
	public ArrayList<NoteFrais> getNoteFrais() {
		return noteFrais;
	}
	
	
	

	
	
}

package com.comm.fr;

import java.util.Date;

public class NoteRepas extends NoteFrais {
	private int prix;

	public NoteRepas(Date date, Commercial commercial, int prix) {
		super(date, commercial);
		this.prix = prix;
		// TODO Auto-generated constructor stub
		
		
	}
	
	public int getRemboursementNoteFrais() {
		
		int price = 0;
		
		if (getCommercial().getCategorie() == 'A') {
			price = 25;
			
		}else if (getCommercial().getCategorie() == 'B') {
			price = 22;
			
		}else if (getCommercial().getCategorie() == 'C') {
			price = 20;
			
		}else {
			price  = getPrix();
		}
	
		
		return price ;
	}

	/**
	 * @return the prix
	 */
	public int getPrix() {
		return prix;
	}

	
	
}

/**
 * 
 */
package com.comm.fr;

import static org.junit.jupiter.api.DynamicTest.stream;

import java.util.ArrayList;

/**
 * @author bzermelwall
 *
 */
public class ServiceCommercial {
	private ArrayList<Commercial> listeDesCommerciaux;

	public ServiceCommercial() {
		listeDesCommerciaux = new ArrayList<>();
	
	}
	
	
	public void setCommercial(Commercial commercial) {
		listeDesCommerciaux.add(commercial);
		
	}
	
	public double getNombreFraisRemboursés() {
		
		//listeDesCommerciaux.stream().filter( stream-> stream.getNoteFrais().forEach()).);)
		double allFrais = 0;
		for (int i = 0; i < listeDesCommerciaux.size(); i++) {
			for(int b = 0 ; b < listeDesCommerciaux.get(i).getNoteFrais().size(); b++) {
				allFrais += listeDesCommerciaux.get(i).getNoteFrais().get(b).calculMontant();
			}
			
		}
		return allFrais;
	}
	

}

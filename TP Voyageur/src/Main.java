import java.util.Date;

import com.comm.fr.Commercial;
import com.comm.fr.Console;
import com.comm.fr.NoteFrais;

/**
 * 
 */

/**
 * @author bzermelwall
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Commercial c;
		c = new Commercial("Jean", "Dupond", 25, 'A');
		NoteFrais noteFrais = new NoteFrais(new Date(2018, 05, 17), c);
		Console.writeLine(noteFrais.toString());
		Console.writeLine(c.toString());
		

	}

}

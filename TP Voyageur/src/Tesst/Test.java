package Tesst;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;

import com.comm.fr.Commercial;
import com.comm.fr.NoteFrais;

class Test {

	@BeforeEach
	void setUp() throws Exception {
	}

	@org.junit.jupiter.api.Test
	void test() {
		Commercial commercial = new Commercial("Kevin","Baudry", 25, 'A');
		NoteFrais f,f1;
		f = new NoteFrais(new Date(2018,05,04), commercial);
		f1  = new NoteFrais(new Date(2018,05,04), commercial);
		assertEquals(2, commercial.getNoteFrais().size());
		
	}

}

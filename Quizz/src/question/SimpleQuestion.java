/**
 * 
 */
package question;

import java.util.Scanner;

import javax.xml.ws.Response;

/**
 * @author bzermelwall
 *
 */
public abstract class SimpleQuestion {
	protected int point;
	protected String question;
	protected Object reponse;
	protected Object choixUser;
	

	/**
	 * @param point
	 * @param question
	 * @param choix 
	 * @param choix
	 * @param reponse
	 */
	protected SimpleQuestion(int point, String question, Object reponse) {
		this.point = point;
		this.question = question;
		this.reponse = reponse;
	}


	public int play(Object object) {
				
		// On adopte la methode a appellé selon la reponse prévu
		if (reponse.getClass().equals(Integer.class)) {
			try {
				choixUser = Integer.parseUnsignedInt((String) object);
			}catch (NumberFormatException e) {
				choixUser = object;
			}
			
		}else {
			choixUser = object;
		}
		
		  
//		 System.out.println("Votre reponse a été prise");
		if (checkReponse()) {
			System.out.println("Bonne réponse");
			return point;
		}
		
		System.out.println("Mauvaise réponse");
		return 0;
	}
	

	

	public boolean checkReponse() {

		if (choixUser.equals(reponse)) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * @return the point
	 */
	protected int getPoint() {
		return point;
	}
	/**
	 * @return the question
	 */
	public void getEnnonce() {
		System.out.println(question);
	}


	/**
	 * @return the choixUser
	 */
	protected Object getChoixUser() {
		return choixUser;
	}


	/**
	 * @param choixUser the choixUser to set
	 */
	protected void setChoixUser(Object choixUser) {
		this.choixUser = choixUser;
	}
	
	

	
}

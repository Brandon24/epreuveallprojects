/**
 * 
 */
package question;

import java.util.Scanner;

/**
 * @author bzermelwall
 * @param <T>
 *
 */
public class MultipleChoixQuestion extends SimpleQuestion {
	private Object [] tabChoix;

	public MultipleChoixQuestion(int point, String question, Object reponse, Object choix[]) {
		super(point, question, reponse);
		this.tabChoix = choix;
		
	}

	
	@Override
	public void getEnnonce() {
		super.getEnnonce();
		for (int i = 0; i < tabChoix.length; i++) {
			System.out.println(tabChoix[i]);
		}
	}

	/* (non-Javadoc)
	 * @see question.SimpleQuestion#checkReponse()
	 */
	@Override
	public boolean checkReponse() {
		for (int i = 0; i < tabChoix.length; i++) {
			if (tabChoix[i].equals(choixUser) ) {
				return true;
			}
		}
		return false;
	}

}

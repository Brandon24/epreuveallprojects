/**
 * 
 */
package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import question.MultipleChoixQuestion;
import question.QuestionA;
import question.SimpleQuestion;

/**
 * @author bzermelwall
 *
 */
public class Main {

	/**
	 * @param <T>
	 * @param args
	 */
	public static <T> void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleQuestion questionA = new QuestionA(5, "Qui est le plus fort ? ", "Moi");
		SimpleQuestion questionB = new QuestionA(5, "Quel est mon nombre favoris ? ", 10);
		String questionMultiple = "Combien de dents avons nous ? ";
		
		String choix[] = {"24","25","26"};
		
		SimpleQuestion multipleChoixQuestion = 
				new MultipleChoixQuestion(2, questionMultiple, "25", choix);
		
		int totalPoint = 0;
		
		
		List<SimpleQuestion> aa = new ArrayList<>();
		aa.add( questionA);
		aa.add( questionB);
		aa.add( multipleChoixQuestion);

		Scanner sc = new Scanner(System.in);
		
		for (int i = 0; i < aa.size(); i++) {
			aa.get(i).getEnnonce();
			
			totalPoint += aa.get(i).play(sc.nextLine());
			
			System.out.println();
		}
		
		System.out.println("Vous avez effectué un total de " + totalPoint + " points");
		

		

	}

}

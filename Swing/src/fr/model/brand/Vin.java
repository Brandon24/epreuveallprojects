/**
 * 
 */
package fr.model.brand;

/**
 * @author Brandon
 *
 */
public class Vin {
	private int vinNum;
	private String vinMilles;
	private String vinQualite;
	private Cru cru;
	
	/**
	 * @param vinNum
	 * @param vinMilles
	 * @param vinQualite
	 * @param cru
	 */
	public Vin(int vinNum, String vinMilles, String vinQualite, Cru cru) {
		this.vinNum = vinNum;
		this.vinMilles = vinMilles;
		this.vinQualite = vinQualite;
		this.cru = cru;
	}

	/**
	 * @return the vinNum
	 */
	public int getVinNum() {
		return vinNum;
	}

	/**
	 * @param vinNum the vinNum to set
	 */
	public void setVinNum(int vinNum) {
		this.vinNum = vinNum;
	}

	/**
	 * @return the vinMilles
	 */
	public String getVinMilles() {
		return vinMilles;
	}

	/**
	 * @param vinMilles the vinMilles to set
	 */
	public void setVinMilles(String vinMilles) {
		this.vinMilles = vinMilles;
	}

	/**
	 * @return the vinQualite
	 */
	public String getVinQualite() {
		return vinQualite;
	}

	/**
	 * @param vinQualite the vinQualite to set
	 */
	public void setVinQualite(String vinQualite) {
		this.vinQualite = vinQualite;
	}

	/**
	 * @return the cru
	 */
	public Cru getCru() {
		return cru;
	}

	/**
	 * @param cru the cru to set
	 */
	public void setCru(Cru cru) {
		this.cru = cru;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Vin [vinNum=" + vinNum + ", vinMilles=" + vinMilles + ", vinQualite=" + vinQualite + ", cru=" + cru
				+ "]";
	}
	
	
	
	
}

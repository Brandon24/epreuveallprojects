package fr.model.brand;

public class Region {
	private int regionNum;
	private String regionNom;
	
	/**
	 * @param regionNum
	 * @param regionNom
	 */
	public Region(int regionNum, String regionNom) {
		super();
		this.regionNum = regionNum;
		this.regionNom = regionNom;
	}

	/**
	 * @return the regionNum
	 */
	public int getRegionNum() {
		return regionNum;
	}

	/**
	 * @param regionNum the regionNum to set
	 */
	public void setRegionNum(int regionNum) {
		this.regionNum = regionNum;
	}

	/**
	 * @return the regionNom
	 */
	public String getRegionNom() {
		return regionNom;
	}

	/**
	 * @param regionNom the regionNom to set
	 */
	public void setRegionNom(String regionNom) {
		this.regionNom = regionNom;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Region [regionNum=" + regionNum + ", regionNom=" + regionNom + "]";
	}



}

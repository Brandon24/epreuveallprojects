/**
 * 
 */
package fr.model.brand;

/**
 * @author Brandon
 *
 */
public class Client {
	
	private int cliNum;
	private String cliNom;
	
	/**
	 * @param cliNum
	 * @param string
	 */
	public Client(int cliNum, String string) {
	
		this.cliNum = cliNum;
		this.cliNom = string;
	}

	/**
	 * @return the cliNum
	 */
	public int getCliNum() {
		return cliNum;
	}

	/**
	 * @param cliNum the cliNum to set
	 */
	public void setCliNum(int cliNum) {
		this.cliNum = cliNum;
	}

	/**
	 * @return the cliNom
	 */
	public String getCliNom() {
		return cliNom;
	}

	/**
	 * @param cliNom the cliNom to set
	 */
	public void setCliNom(String cliNom) {
		this.cliNom = cliNom;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [cliNum=" + cliNum + ", cliNom=" + cliNom + "]";
	}
	
	

}

/**
 * 
 */
package fr.model.brand;

/**
 * @author Brandon
 *
 */
public class Cru {
	private int cruCode;
	private String cruNom;
	private Region region;
	/**
	 * @param cruCode
	 * @param cruNom
	 * @param region
	 */
	public Cru(int cruCode, String cruNom, Region region) {
		this.cruCode = cruCode;
		this.cruNom = cruNom;
		this.region = region;
	}
	/**
	 * @return the cruCode
	 */
	public int getCruCode() {
		return cruCode;
	}
	/**
	 * @param cruCode the cruCode to set
	 */
	public void setCruCode(int cruCode) {
		this.cruCode = cruCode;
	}
	/**
	 * @return the cruNom
	 */
	public String getCruNom() {
		return cruNom;
	}
	/**
	 * @param cruNom the cruNom to set
	 */
	public void setCruNom(String cruNom) {
		this.cruNom = cruNom;
	}
	/**
	 * @return the region
	 */
	public Region getRegion() {
		return region;
	}
	/**
	 * @param region the region to set
	 */
	public void setRegion(Region region) {
		this.region = region;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Cru [cruCode=" + cruCode + ", cruNom=" + cruNom + ", region=" + region + "]";
	}
	
	

}

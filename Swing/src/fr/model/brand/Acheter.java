package fr.model.brand;
/**
 * @author Brandon
 *
 */

public class Acheter {
	private Client client;
	private Vin vin;
	private int sem_Numero;
	private int achQuantite;
	
	/**
	 * @param client
	 * @param vin
	 * @param sem_Numero
	 * @param achQuantite
	 */
	public Acheter(Client client, Vin vin, int sem_Numero, int achQuantite) {
		this.client = client;
		this.vin = vin;
		this.sem_Numero = sem_Numero;
		this.achQuantite = achQuantite;
	}

	/**
	 * @return the vin
	 */
	public Vin getVin() {
		return vin;
	}

	/**
	 * @param vin the vin to set
	 */
	public void setVin(Vin vin) {
		this.vin = vin;
	}

	/**
	 * @return the sem_Numero
	 */
	public int getSem_Numero() {
		return sem_Numero;
	}

	/**
	 * @param sem_Numero the sem_Numero to set
	 */
	public void setSem_Numero(int sem_Numero) {
		this.sem_Numero = sem_Numero;
	}

	/**
	 * @return the achQuantite
	 */
	public int getAchQuantite() {
		return achQuantite;
	}

	/**
	 * @param achQuantite the achQuantite to set
	 */
	public void setAchQuantite(int achQuantite) {
		this.achQuantite = achQuantite;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Acheter [client=" + client + ", vin=" + vin + ", sem_Numero=" + sem_Numero + ", achQuantite="
				+ achQuantite + "]";
	}
	
	
	
	
	
	

}

/**
 * 
 */
package fr.listener.brand;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JFrame;

import fr.dao.brand.ConnectDAO;

/**
 * @author Brandon
 *
 */
public class EcouteurFenetre extends WindowAdapter {
	private JFrame frame;
	
	public EcouteurFenetre(JFrame frame) {
		this.frame = frame;
		
	}
	
	/*
	 *  Permet la fermeture de la base lorsque l'utilisateur d�cide de fermer la Fenetre
	 *  (non-Javadoc)
	 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent e){
		 try {
			ConnectDAO.getConnect().close();
			System.out.println("Fin de la connexion a la base...");
			// d�truit la jframe
			frame.dispose();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		   
	}


}

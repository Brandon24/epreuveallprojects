/**
 * 
 */
package fr.dao.brand;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Brandon
 *
 */
public abstract class ConnectDAO <A> {
	
	private final static String USER = "mysql";
	private final static String PASSWORD ="mysql";
	private final static String URL = "jdbc:mysql://localhost:3306/tp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	
	private static  Connection connect = null;
	protected ResultSet resulset;
	protected Statement statement;

	
	public abstract A find(int id);
	
	public abstract boolean update(A object);
	
	/** Singleton
	 * @return the connect
	 */
	public static Connection getConnect() {
		
		if(connect == null) {
			try {
				connect = DriverManager.getConnection(URL, USER, PASSWORD);
				System.out.println("Connexion R�ussi !");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return connect;
	}
	
}

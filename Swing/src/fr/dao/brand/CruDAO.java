/**
 * 
 */
package fr.dao.brand;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import fr.model.brand.Cru;
import fr.model.brand.Region;

/**
 * @author Brandon
 *
 */
public class CruDAO extends ConnectDAO<Cru> {

	@Override
	public Cru find(int id) {
		Cru cru = null;
		Region region;
		
		try {	
			statement = getConnect().createStatement();
			resulset =	statement.executeQuery("select * from cru where cru_code = " + id);
			resulset.next();
		
			region = new RegionDAO().find(resulset.getInt("reg_num"));
			cru = new Cru(resulset.getInt(1), resulset.getString("cru_nom"),region);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cru;
	}

	@Override
	public boolean update(Cru object) {
		try {
			PreparedStatement update =  
					getConnect().prepareStatement("update cru set cru_nom = ?  where cru_code = ? ");
			update.setString(1, object.getCruNom());
			update.setInt(2, object.getCruCode());
			update.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}

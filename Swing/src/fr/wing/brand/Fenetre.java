/**
 * 
 */
package fr.wing.brand;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import fr.dao.brand.ConnectDAO;
import fr.listener.brand.EcouteurFenetre;

/**
 * @author Brandon
 *
 */
public class Fenetre extends JFrame {
	
	private final int WIDTH = 400;
	private final int HEIGHT = 400;
	private final String nomApplication = "Gestion de vin";
	
	/*
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Fenetre()  {
		
		JTable table = new JTable(new TableauModel());
		JLabel informationModif = new JLabel("Les modifications sont directement chargés dans la base ! Aucune validation nécessaire !");
		informationModif.setForeground(Color.RED);
		
		// Va nous permettre de fermer la connexion a la base lors de la fermeture a la fenetre 
		this.addWindowListener(new EcouteurFenetre(this));
		this.add(informationModif,BorderLayout.NORTH);
		this.add(new JScrollPane(table));
		
		this.setTitle(nomApplication);
		this.setSize(WIDTH, HEIGHT);
		this.setBackground(Color.ORANGE);
		this.pack();
		this.setVisible(true);
	
	}
	


}

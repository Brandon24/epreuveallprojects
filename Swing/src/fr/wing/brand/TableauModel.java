/**
 * 
 */
package fr.wing.brand;

import java.sql.SQLException;
import java.util.Vector;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import fr.dao.brand.AcheterDAO;
import fr.model.brand.Acheter;

/**
 * @author Brandon
 *
 */
public class TableauModel extends AbstractTableModel implements TableModelListener  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title[] = new String [] 
			{"Cli_Nom","Cru_Nom","Vin_Milles","Vin_Qualite", "Reg_Nom","Ach_Qte"};
	
	private Vector<Acheter> listAchat;

	private AcheterDAO acheterdao = new AcheterDAO();
	
	
	public TableauModel() {
		try {
			listAchat = acheterdao.findAllResult();
			
		} catch (SQLException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		addTableModelListener(this);
	
	}
	
	
	/* Methode donnant le nombre de ligne
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		
		return listAchat.size();
	}
	

	/*  Method donnant le nombre de colonne
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {

		return title.length;
	}
	
	public String getColumnName(int col) {
		return title[col];
		
	}
	

	/* Methode permettant d'attribuer les valeurs dans les colonnes correspondes
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
	
		 switch (columnIndex) {
			case 0:
				return  listAchat.get(rowIndex).getClient().getCliNom();
			
			case 1:
				return  listAchat.get(rowIndex).getVin().getCru().getCruNom();
			
			case 2:
				return  listAchat.get(rowIndex).getVin().getVinMilles();
			
			case 3 :
				return  listAchat.get(rowIndex).getVin().getVinQualite();
			
			case 4 :
				return  listAchat.get(rowIndex).getVin().getCru().getRegion().getRegionNom();
			
			case 5 :
				return  listAchat.get(rowIndex).getAchQuantite();
			default:
				return null;
		 }

	}

	/* Methode permettant le changement de valeur dans la table lorsque l'utilisateur modifie
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	 @Override
	    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
	    {
	    
	        // 0,1,2,3,4,5 correspondent aux colonnes
		 switch (columnIndex) {
			case 0:
				listAchat.get(rowIndex).getClient().setCliNom(aValue.toString());
				break;
			case 1:
				listAchat.get(rowIndex).getVin().getCru().setCruNom(aValue.toString());;
				break;
			case 2:
				listAchat.get(rowIndex).getVin().setVinMilles(aValue.toString());;
				break;
			case 3 :
				listAchat.get(rowIndex).getVin().setVinQualite(aValue.toString());;
				break;
			case 4 :
				listAchat.get(rowIndex).getVin().getCru().getRegion().setRegionNom(aValue.toString());;
				break;
			case 5 :
				 if (isInteger(aValue)) 
					 listAchat.get(rowIndex).setAchQuantite(Integer.parseInt(aValue.toString())); 
				 else 
					 listAchat.get(rowIndex).setAchQuantite(listAchat.get(rowIndex).getAchQuantite());
				break;
		 }      
		 
		 	fireTableCellUpdated(rowIndex, columnIndex);

	    }
	
	 /* Methode permettant de rendre les cellules editables
	  * (non-Javadoc)
	  * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	  * @return boolean
	  */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		
		return true;
	}

	/*
	 * Method permettant de savoir quel ligne a �t� modifi� et d'update le changement en base
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	@Override
	public void tableChanged(TableModelEvent e) {
		// update toutes les tables
	
			System.out.println("Update en cours...");
		
			acheterdao.update(listAchat.get(e.getLastRow()));
			
			System.out.println("Update finis");
			
		
	}
	
	public boolean isInteger(Object value) {
		try {
			Integer.parseInt(value.toString());
		} catch (NumberFormatException error) {
			// TODO: handle exception
			error.printStackTrace();
			return false;
		}
		
		return true;
	}

}

package test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.model.brand.Region;

/**
 * 
 */

/**
 * @author Brandon
 *
 */
class RegionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		
	}

	/**
	 * Test method for {@link fr.model.brand.Region#Region(int, java.lang.String)}.
	 */
	@Test
	void testRegion() throws Exception {
		Region region = new Region(10, "&�='�)");
	}

	/**
	 * Test method for {@link fr.model.brand.Region#getRegionNum()}.
	 */
	@Test
	void testGetRegionNum() {
		assertNotEquals(0, new Region(5, "").getRegionNum());
	}

	/**
	 * Test method for {@link fr.model.brand.Region#getRegionNom()}.
	 */
	@Test
	void testGetRegionNom() {
		assertNotEquals(new Region(2, "Normandie").getRegionNom(), new Region(4, "Dordogne").getRegionNom());
	}

}

package test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.model.brand.Acheter;
import fr.model.brand.Client;
import fr.model.brand.Cru;
import fr.model.brand.Region;
import fr.model.brand.Vin;

/**
 * @author Brandon
 *
 */
class AcheterTest {
	Client client;
	Vin vin;
	Cru cru;
	Region region;
	Acheter acheter;
	
	@BeforeEach
	void setUp() {
		client = new Client(1, "Fontenoy");
		
		region = new Region(1, "Bretagne");
		
		cru = new Cru(1, "Inconnu", region);
				
		vin = new Vin(1, "Parfait", "M", cru);
		
		acheter = new Acheter(client, vin, 3, 1000);
		
	}
	

	/**
	 * Test method for {@link fr.model.brand.Acheter#getVin()}.
	 */
	@Test
	void testGetVin() {
		assertEquals("M", acheter.getVin().getVinQualite());
	}
	

	/**
	 * Test method for {@link fr.model.brand.Acheter#getAchQuantite()}.
	 */
	@Test
	void testGetAchQuantite() {
		assertEquals(1000, acheter.getAchQuantite());
	}

	/**
	 * Test method for {@link fr.model.brand.Acheter#setAchQuantite(int)}.
	 */
	@Test
	void testSetAchQuantite() {
		Acheter m = new Acheter(client, vin, 1, 100);
		m.setAchQuantite(1500);
		assertNotNull(m.getAchQuantite());
	
	}

}

package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.model.brand.Cru;
import fr.model.brand.Region;

/**
 * 
 */

/**
 * @author Brandon
 *
 */
class CruTest {

	Region region;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		region = new fr.model.brand.Region(1, "Bretagne");
	
	}

	/**
	 * Test method for {@link fr.model.brand.Cru#Cru(int, java.lang.String, fr.model.brand.Region)}.
	 */
	@Test
	void testCru()  throws Exception {
		Cru cru = new Cru(1, "", region);
	}

	/**
	 * Test method for {@link fr.model.brand.Cru#getCruCode()}.
	 */
	@Test
	void testGetCruCode() {
		Cru cru = new Cru(1, "", region);
		
		assertSame(1, cru.getCruCode());
		
	}

	/**
	 * Test method for {@link fr.model.brand.Cru#getRegion()}.
	 */
	@Test
	void testGetRegion() {
		assertNotEquals(new Region(2, "Normandie").getRegionNom(), region.getRegionNom());
	}

}

package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.model.brand.Client;

/**
 * 
 */

/**
 * @author Brandon
 *
 */
class ClientTest {

	Client first;
	Client second;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		first = new Client(1, "Fontenoy");
		second = new Client(-10,"Skinny");
	}


	/**
	 * Test method for {@link fr.model.brand.Client#getCliNum()}.
	 */
	@Test
	void testGetCliNum() {
		assertSame(1, first.getCliNum());
		assertNotNull(second.getCliNum());
	}

	/**
	 * Test method for {@link fr.model.brand.Client#getCliNom()}.
	 */
	@Test
	void testGetCliNom() {
		assertEquals("Fontenoy", first.getCliNom());
		assertNotNull(second.getCliNom());
	}

}

/**
 * 
 */
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.model.brand.Vin;

/**
 * @author Brandon
 *
 */
class VinTest {
	

	/**
	 * Test method for {@link fr.model.brand.Vin#Vin(int, java.lang.String, java.lang.String, fr.model.brand.Cru)}.
	 */
	@Test
	void testVin() throws Exception {
		Vin x = new Vin(1, "", "", null);
	}

	/**
	 * Test method for {@link fr.model.brand.Vin#getVinNum()}.
	 */
	@Test
	void testGetVinNum() {
		assertNotEquals(100, new Vin(4545, "", "", null));
	}

	/**
	 * Test method for {@link fr.model.brand.Vin#setVinNum(int)}.
	 */
	@Test
	void testSetVinNum() {
		Vin vin = new Vin(4, "", "", null);	
		
		vin.setVinNum(454);
		
		assertEquals(454, vin.getVinNum());
	}


}

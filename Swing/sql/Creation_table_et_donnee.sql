-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 09 Octobre 2018 à 19:29
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

CREATE DATABASE tp;
USE tp;


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `DAO`
--

-- --------------------------------------------------------

--
-- Structure de la table `acheter`
--

CREATE TABLE `acheter` (
  `CLI_NUM` int(11) DEFAULT NULL,
  `VIN_NUM` int(11) DEFAULT NULL,
  `SEM_NUMERO` int(11) NOT NULL,
  `ACH_QTE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `acheter`
--

INSERT INTO `acheter` (`CLI_NUM`, `VIN_NUM`, `SEM_NUMERO`, `ACH_QTE`) VALUES
(1001, 1, 15, 30010),
(1002, 2, 16, 6000);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `CLI_NUM` int(11) NOT NULL,
  `CLI_NOM` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`CLI_NUM`, `CLI_NOM`) VALUES
(1001, 'Julie'),
(1002, 'Bastien'),
(1003, 'Tulipe');

-- --------------------------------------------------------

--
-- Structure de la table `cru`
--

CREATE TABLE `cru` (
  `CRU_CODE` int(11) NOT NULL,
  `CRU_NOM` varchar(20) DEFAULT NULL,
  `REG_NUM` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cru`
--

INSERT INTO `cru` (`CRU_CODE`, `CRU_NOM`, `REG_NUM`) VALUES
(1, 'Champignon', 16300),
(2, 'Mauvais', 24000),
(3, 'Mooeulloeux', 16300);

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `REG_NUM` int(11) NOT NULL,
  `REG_NOM` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `region`
--

INSERT INTO `region` (`REG_NUM`, `REG_NOM`) VALUES
(16300, 'Charente'),
(24000, 'Aquitaine'),
(30000, 'Gard');

-- --------------------------------------------------------

--
-- Structure de la table `vin`
--

CREATE TABLE `vin` (
  `VIN_NUM` int(11) NOT NULL,
  `VIN_QUALITE` varchar(10) DEFAULT NULL,
  `CRU_CODE` int(11) DEFAULT NULL,
  `VIN_MILLES` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vin`
--

INSERT INTO `vin` (`VIN_NUM`, `VIN_QUALITE`, `CRU_CODE`, `VIN_MILLES`) VALUES
(1, '5', 1, 'Gout'),
(2, '11', 3, 'Jambon');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acheter`
--
ALTER TABLE `acheter`
  ADD PRIMARY KEY (`SEM_NUMERO`),
  ADD KEY `CLI_NUM` (`CLI_NUM`),
  ADD KEY `VIN_NUM` (`VIN_NUM`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`CLI_NUM`);

--
-- Index pour la table `cru`
--
ALTER TABLE `cru`
  ADD PRIMARY KEY (`CRU_CODE`),
  ADD KEY `REG_NUM` (`REG_NUM`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`REG_NUM`);

--
-- Index pour la table `vin`
--
ALTER TABLE `vin`
  ADD PRIMARY KEY (`VIN_NUM`),
  ADD KEY `CRU_CODE` (`CRU_CODE`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acheter`
--
ALTER TABLE `acheter`
  ADD CONSTRAINT `acheter_ibfk_1` FOREIGN KEY (`CLI_NUM`) REFERENCES `client` (`CLI_NUM`),
  ADD CONSTRAINT `acheter_ibfk_2` FOREIGN KEY (`VIN_NUM`) REFERENCES `vin` (`VIN_NUM`);

--
-- Contraintes pour la table `cru`
--
ALTER TABLE `cru`
  ADD CONSTRAINT `cru_ibfk_1` FOREIGN KEY (`REG_NUM`) REFERENCES `region` (`REG_NUM`);

--
-- Contraintes pour la table `vin`
--
ALTER TABLE `vin`
  ADD CONSTRAINT `vin_ibfk_1` FOREIGN KEY (`CRU_CODE`) REFERENCES `cru` (`CRU_CODE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
